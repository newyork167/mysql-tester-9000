import argparse
import json
import re
import os
import itertools
import configuration as config
from sqlalchemy import create_engine

engine = create_engine('mysql://{username}:{password}@{hostname}/{database}'.format(
    username=config.get('mysql', 'username'),
    password=config.get('mysql', 'password'),
    hostname=config.get('mysql', 'hostname'),
    database=config.get('mysql', 'database')
))

debug = False
output_file = open('output.txt', 'w+')
output_str = '{separator}\n--{filename}\n{str}\n{separator}\n'
separator = '-' * 50

unit_test_file = open('tests.py', 'w+', encoding='utf-8')
temporary_output_file = open('temp.txt', 'w+', encoding='utf-8')
error_log_file = open('error.txt', 'w+', encoding='utf-8')

data_types_set = set()

test_variable = {}
test_variable['str'] = '\"abcdefghijklmnopqrstuvwxyz\"'
test_variable['int'] = '1234567890987654321'
test_variable['datetime'] = '01/01/2077'
test_variable['timestamp'] = ''
test_variable['bool'] = False

table_schemas = {}
temp_table_schemas = {}

insert_statement = '\'INSERT INTO {table} SET {columns}\''
sql_statement = '\'{sql}\''
select_statement = '\'SELECT {columns} FROM {table} {where_clause}\''
delete_statement = '\'DELETE FROM {table} {where_clause}\''


def output_to_output_file(filename, str_to_write):
    output_file.write(output_str.format(separator=separator, filename=filename, str=str_to_write))
    print(output_str.format(separator=separator, filename=filename, str=str_to_write))
    output_file.flush()


def output_to_error_log(s):
    error_log_file.write(s + '\n')
    error_log_file.flush()


def output_to_unit_test_file(str_to_write):
    unit_test_file.write(str_to_write.rstrip() + '\n')
    unit_test_file.flush()


def setup_unit_test_file():
    output_to_unit_test_file("import sqlalchemy")
    output_to_unit_test_file("from sqlalchemy import create_engine")

    output_to_unit_test_file('')

    output_to_unit_test_file("output_file = open('output.txt', 'w+')\n\n")

    output_to_unit_test_file("engine = create_engine('mysql://{username}:{password}@{hostname}/{database}')".format(
        username=config.get('mysql', 'username'),
        password=config.get('mysql', 'password'),
        hostname=config.get('mysql', 'hostname'),
        database=config.get('mysql', 'database')
    ))

    output_to_unit_test_file('\n')

    output_to_unit_test_file("""
def output_to_file(s):
    output_file.write(str(s))
    output_file.write('\\n')
    """)

    output_to_unit_test_file('\n')

    output_to_unit_test_file('with engine.connect() as con:')
    output_to_unit_test_file('\n')


def get_clause(sql, clause):
    split_sql = [' '.join(items) for _, items in itertools.groupby(sql.split(), str.isupper)]
    try:
        if clause in split_sql:
            return split_sql[split_sql.index(clause) + 1].strip()
    except Exception as ex:
        pass
    return ""


def get_where_clause(sql):
    return get_clause(sql, 'WHERE')


def get_having_clause(sql):
    return get_clause(sql, 'HAVING')


def get_order_by_clause(sql):
    return get_clause(sql, 'ORDER BY')


def get_table_name(sql):
    table_name = ''

    statement_type = sql.split(' ')[0]

    if statement_type == 'SELECT':
        table_name = get_clause(sql=sql, clause='FROM')
    elif statement_type == 'UPDATE':
        table_name = get_clause(sql=sql, clause='UPDATE')
    elif statement_type == 'DELETE':
        table_name = get_clause(sql=sql, clause='DELETE FROM')
    elif statement_type == 'INSERT':
        table_name = get_clause(sql=sql, clause='INSERT INTO')

    return table_name.strip()


def get_column_info(sql):
    columns = []

    statement_type = sql.split(' ')[0]

    if statement_type == 'SELECT':
        columns = get_clause(sql, 'SELECT').split(',')
    elif statement_type in ['UPDATE', 'INSERT']:
        columns = [x.split('=')[0].strip() for x in get_clause(sql, 'SET').split(',')]

    return columns


def get_where_info(sql):
    where_clause = get_where_clause(sql=sql)

    columns = [x.split('=')[0].strip() for x in where_clause.split(',') if x]

    return columns


def parse_strings(text):
    # Pattern to find all strings in file
    pattern = r'"(.*?)"'

    text = text.replace('\" .', '\' .').replace('. \"', '. \'')

    sql_to_check = ['SELECT', 'UPDATE', 'INSERT', 'DELETE', 'ORDER', 'HAVING', 'INNER', 'OUTER', 'JOIN']

    # Return all strings
    potential_sql_statements = [x for x in re.findall(pattern=pattern, string=text) if
                                x and any(word in x for word in sql_to_check)]

    sql_return_statements = []

    for sql_statement in potential_sql_statements:
        first_word = sql_statement.split(' ')[0]

        if first_word in sql_to_check:
            sql_return_statements += [sql_statement.split(';')[0]]

    return sql_return_statements


def get_table_schema(table_name):
    column_info = {}
    temp_column_info = {}

    try:
        with engine.connect() as con:
            column_data = con.execute('DESCRIBE {table}'.format(table=table_name))
            for column in column_data:

                data_types_set.add(column[1])

                data_type = column[1].split('(')[0]

                # 'enum(\\'sales_order_bin\\',\\'inventory_bin\\',\\'ltl_area\\',\\'other\\')'
                if data_type == 'enum':
                    column_info[column[0]] = column[1].split('(')[1].split(',')[0].replace('\'', '"')
                else:
                    if data_type == 'int':
                        int_length = int(column[1][column[1].find("(")+1:column[1].find(")")])
                        if int_length:
                            sql_test_variable = int(test_variable['int'][:int_length])
                        else:
                            sql_test_variable = int(test_variable['int'])
                    elif data_type == 'varchar':
                        varchar_length = int(column[1][column[1].find("(")+1:column[1].find(")")])
                        sql_test_variable = test_variable['str'][:varchar_length]
                        sql_test_variable += '\"' if sql_test_variable[-1] != '\"' else ''
                    else:
                        sql_test_variable = 'test'

                    sql_test_variable = sql_test_variable.replace('\'', '\"')

                    column_info[column[0]] = sql_test_variable
                    temp_column_info[column[0]] = sql_test_variable

            temp_table_schemas[table_name] = temp_column_info

        temporary_output_file.write(json.dumps(column_info))
    except Exception as ex:
        output_to_error_log(str(ex))
        temporary_output_file.write(str(ex))

    return column_info


def is_sql_mathematical(sql):
    for i in sql.split():
        if i in ['+', '-', '*', '%'] or i.isdigit():
            return True
    return False


def fix_sql_statement(sql):
    statement_type = sql.split(' ')[0]

    # Remove everything between single quotes to get rid of php variables
    sql = re.sub('\'[^\']+\'', '', sql)

    if sql.count('\'') % 2 == 1:
        sql = sql.split('\'')[0]

    table_name = get_table_name(sql=sql)

    if not table_name:
        output_to_error_log('No table name: {}'.format(sql))
        return sql
    elif 'information_schema' in table_name:
        output_to_error_log('Cannot insert to information schema')
        return sql

    columns_info = get_column_info(sql=sql)
    where_columns_info = get_where_info(sql=sql)
    having_clause_info = get_having_clause(sql=sql)

    if table_name not in table_schemas.keys():
        table_schemas[table_name] = get_table_schema(table_name=table_name)
    where_columns = ', '.join(['{column} = {variable}'.format(column=x, variable=table_schemas[table_name][x] if x in table_schemas[table_name] else 'test') for x in where_columns_info])

    # where_columns = ', '.join(
    #     ['{column} = {variable}'.format(column=x, variable=test_variable_str) for x in where_columns_info])
    where_columns = '' if len(where_columns) > 0 else 'WHERE {}'.format(where_columns)

    having_clause_info = '' if not having_clause_info else 'HAVING {}'.format(having_clause_info)

    if statement_type == 'SELECT':
        select_columns = ','.join(columns_info)
        return 'SELECT {columns} FROM {table} {where_clause} {having_clause}'.format(columns=select_columns,
                                                                                     table=table_name,
                                                                                     where_clause=where_columns,
                                                                                     having_clause=having_clause_info)
    elif statement_type == 'UPDATE':
        update_columns = ', '.join(
            ['{column} = {variable}'.format(column=x, variable=table_schemas[table_name][x]) for x in columns_info])
        return 'UPDATE {table} SET {columns} {where_clause}'.format(columns=update_columns, table=table_name,
                                                                    where_clause=where_columns)
    elif statement_type == 'DELETE':
        return 'DELETE FROM {table} {where_clause}'.format(table=table_name, where_clause=where_columns)
    elif statement_type == 'INSERT':
        insert_columns = ', '.join(['{column} = {variable}'.format(column=x, variable=table_schemas[table_name][x] if x in table_schemas[table_name] else 'test') for x in columns_info])
        return 'INSERT INTO {table} SET {columns}'.format(columns=insert_columns, table=table_name)
    return ''


def build_select_test_case(sql_query, delete_query, table_name, select_columns, insert_columns):
    if is_sql_mathematical(select_columns):
        sql_cases = "[\n\t\t\t{}\n\t\t]\n".format(','.join([sql_statement]))
    else:
        insert_query = ' '.join(insert_statement.format(table=table_name, columns=insert_columns).split())
        sql_cases = "[\n\t\t\t{}\n\t\t]\n".format(', \n\t\t\t'.join([insert_query, sql_query, delete_query]))
    test_columns = [[y.replace('"', '').strip() for y in x.split('=')] for x in insert_columns.split(', ')]

    # START UNIT TEST STRING
    unit_test_string = """
    try:
        sql_cases = {sql_cases}
        test_columns = {test_columns}

        # Insert the test row
        con.execute(sql_cases[0])

        # Select the test row
        failed = False
        
        query_results = con.execute(sql_cases[1])
        for qr in query_results:
            if failed:
                break
            for qr_column in len(qr):
                if test_columns[qr_column][1] != qr[qr_column]:
                    failed = True
                    break
        
        if failed:
            output_to_file('FAILED - {{}}'.format(sql_cases[1]))
        else:
            output_to_file('SUCCESS - {{}}'.format(sql_cases[1]))

        # delete the test row
        con.execute(sql_cases[2])

    except Exception as ex:
        output_to_file(ex)
    """.format(
        sql_cases=sql_cases,
        test_columns=test_columns
    )
    # END UNIT TEST STRING

    return unit_test_string


def build_update_test_case(sql_query, select_query, delete_query, table_name, insert_columns):
    insert_query = ' '.join(insert_statement.format(table=table_name, columns=insert_columns).split())
    sql_cases = "[\n\t\t\t{}\n\t\t]\n".format(', \n\t\t\t'.join([insert_query, sql_query, select_query, delete_query]))
    test_columns = [[y.replace('"', '').strip() for y in x.split('=')] for x in insert_columns.split(', ')]

    # START UNIT TEST STRING
    unit_test_string = """
    try:
        sql_cases = {sql_cases}
        test_columns = {test_columns}

        # Insert the test row
        con.execute(sql_cases[0])
        
        # Update the test row
        con.execute(sql_cases[1])

        # Select the test row
        failed = False
        
        query_results = con.execute(sql_cases[2])
        for qr in query_results:
            if failed:
                break
            for qr_column in len(qr):
                if test_columns[qr_column][1] != qr[qr_column]:
                    failed = True
                    break
        
        if failed:
            output_to_file('FAILED - {{}}'.format(sql_cases[1]))
        else:
            output_to_file('SUCCESS - {{}}'.format(sql_cases[1]))

        # delete the test row
        con.execute(sql_cases[3])

    except Exception as ex:
        output_to_file(ex)
        """.format(
        sql_cases=sql_cases,
        test_columns=test_columns
    )
    # END UNIT TEST STRING

    return unit_test_string


def build_delete_test_case(sql_query, select_query, table_name, where_columns):
    insert_query = ' '.join(insert_statement.format(table=table_name, columns=where_columns).split())
    sql_cases = "[\n\t\t\t{}\n\t\t]\n".format(', \n\t\t\t'.join([insert_query, sql_query, select_query]))
    test_columns = [[y.replace('"', '').strip() for y in x.split('=')] for x in where_columns.split(', ')]

    # START UNIT TEST STRING
    unit_test_string = """
    try:
        sql_cases = {sql_cases}
        test_columns = {test_columns}

        # Insert the test row
        con.execute(sql_cases[0])
        
        # delete the test row
        con.execute(sql_cases[1])

        # Select the test row
        failed = False
        
        query_results = con.execute(sql_cases[2])
        failed = not len(query_results) == 0
        
        if failed:
            output_to_file('FAILED - {{}}'.format(sql_cases[1]))
        else:
            output_to_file('SUCCESS - {{}}'.format(sql_cases[1]))

    except Exception as ex:
        output_to_file(ex)
        """.format(
        sql_cases=sql_cases,
        test_columns=test_columns
    )
    # END UNIT TEST STRING

    return unit_test_string


def build_insert_test_case(sql_query, select_query, delete_query, insert_columns):
    sql_cases = "[\n\t\t\t{}\n\t\t]\n".format(', \n\t\t\t'.join([sql_query, select_query, delete_query]))
    test_columns = [[y.replace('"', '').strip() for y in x.split('=')] for x in insert_columns.split(', ')]

    # START UNIT TEST STRING
    unit_test_string = """
    try:
        sql_cases = {sql_cases}
        test_columns = {test_columns}

        # Insert the test row
        con.execute(sql_cases[0])

        # Select the test row
        failed = False
        
        query_results = con.execute(sql_cases[1])
        for qr in query_results:
            if failed:
                break
            for qr_column in len(qr):
                if test_columns[qr_column][1] != qr[qr_column]:
                    failed = True
                    break
        
        if failed:
            output_to_file('FAILED - {{}}'.format(sql_cases[0]))
        else:
            output_to_file('SUCCESS -{{}}'.format(sql_cases[0]))

        # delete the test row
        con.execute(sql_cases[2])

    except Exception as ex:
        output_to_file(ex)
        """.format(
        sql_cases=sql_cases,
        test_columns=test_columns
    )
    # END UNIT TEST STRING

    return unit_test_string


def build_sql_test(sql):
    unit_test_string = ''

    statement_type = sql.split(' ')[0]

    output_to_unit_test_file('\t# {}'.format(sql))

    original_sql = sql

    # Get rid of any appended strings from code
    if original_sql.split()[0] in ['INNER', 'JOIN', 'HAVING', 'ORDER']:
        return None

    sql = fix_sql_statement(sql)

    table_name = get_table_name(sql=sql)

    if not table_name:
        output_to_error_log('No table name: {}'.format(original_sql))
        return None
    elif 'information_schema' in table_name:
        output_to_error_log('Cannot insert to information schema')
        return None

    columns_info = get_column_info(sql=sql)
    where_columns_info = get_where_info(sql=sql)

    if statement_type == 'DELETE':
        columns_info = where_columns_info
    elif statement_type == 'UPDATE' and len(where_columns_info) == 0:
        where_columns_info = columns_info

    select_columns = ','.join(columns_info)
    insert_columns = ', '.join(['{column} = {variable}'.format(column=x.strip(), variable=table_schemas[table_name][x.strip()]) for x in columns_info if x != ''])

    if len(where_columns_info):
        where_columns = ','.join(['{column} = {variable}'.format(column=x.strip(), variable=table_schemas[table_name][x.strip()]) for x in where_columns_info if x != ''])

    # Build out sql statements and split to get rid of excessive spacing
    sql_query = ' '.join(sql_statement.format(sql=sql).split())
    select_query = ' '.join(select_statement.format(table=table_name, columns=select_columns, where_clause='' if not where_columns else 'WHERE {}'.format(where_columns)).split())
    delete_query = ' '.join(delete_statement.format(table=table_name, where_clause='' if not where_columns else 'WHERE {}'.format(where_columns)).split())

    # selects require an insert
    if statement_type == 'SELECT':
        if len(select_columns) == 0:
            output_to_error_log('Select has no columns: {}'.format(original_sql))
        unit_test_string = build_select_test_case(sql_query=sql_query, delete_query=delete_query, table_name=table_name, select_columns=select_columns, insert_columns=insert_columns)

    # updates require an insert and select
    elif statement_type == 'UPDATE':
        unit_test_string = build_update_test_case(sql_query=sql_query, select_query=select_query, delete_query=delete_query, table_name=table_name, insert_columns=insert_columns)

    # deletes require an insert and select
    elif statement_type == 'DELETE':
        unit_test_string = build_delete_test_case(sql_query=sql_query, select_query=select_query, table_name=table_name, where_columns=where_columns)

    # inserts require a select
    elif statement_type == 'INSERT':
        unit_test_string = build_insert_test_case(sql_query=sql_query, select_query=select_query, delete_query=delete_query, insert_columns=insert_columns)

    output_to_unit_test_file(unit_test_string)
    output_to_unit_test_file('\n\n')

    return unit_test_string.split(', ')


def parse_from_specific_file_types(directory, file_extensions=('.php', '.phtml')):
    global debug

    num_files = 0
    num_statements = 0

    print("Checking {}".format(directory))

    debug_set = ['sage100.helper.dao.class.php']

    for root, directories, filenames in os.walk(directory):
        debug = False
        for filename in filenames:
            if filename in debug_set:
                debug = True
            if filename.endswith(file_extensions):
                with open(os.path.join(root, filename), encoding="latin-1") as f:
                    ps = parse_strings(" ".join(f.readlines()).replace('\t', '').replace('\n', '').strip())
                    if len(ps):
                        for sql in ps:
                            try:
                                tests = build_sql_test(sql)
                            except Exception as ex:
                                output_to_error_log(str(ex))
                        num_statements += len(ps)
                        num_files += 1
                        output_to_output_file(os.path.join(root, filename), "\n".join(ps))
    output_file.write('\n' + '=' * 100 + '\n')
    output_file.write('Number of Files: {}\n'.format(num_files))
    output_file.write('Number of SQL Statements: {}\n'.format(num_statements))


def parse_args():
    parser = argparse.ArgumentParser(description="MySQL Parser 9000!", epilog="Written by newyork167")
    parser.add_argument("--input-dir", "-i", help="input directory", default=".")
    return parser


def main():
    parser = parse_args()
    args = parser.parse_args()

    directory = args.input_dir

    setup_unit_test_file()

    parse_from_specific_file_types(directory=directory)

    with open('table_schemas.txt', 'w+', encoding='utf-8') as ts_file:

        ts_file.write("All Data Types: ")

        for dt in sorted(data_types_set):
            ts_file.write(str(dt) + '\n')

        ts_file.write('-' * 50 + '\n')

        for ts in sorted(temp_table_schemas.keys()):
            ts_file.write(ts + ' - ' + json.dumps(temp_table_schemas[ts]))
            ts_file.write('\n' + '-' * 25 + '\n')


if __name__ == "__main__":
    main()
